# youtube-snippets

Simple script for extracting audio snippets from youtube videos.

## Installation and Usage

```sh
pip3 install -r requirements.txt
python3 download_snippets.py
```

All snippets should be in the `snippets` folder.

## Adding a new snippet

Edit the file `snippets.yaml`. Use the `force` flag when adding a new snippet and remove when you are done.

```yaml
<youtube_hash>: # <video name>
  - name: <snippet_name>
    start: 0:01.0
    end: 0:08.0
    force: True
```
