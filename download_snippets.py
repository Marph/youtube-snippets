from pathlib import Path

from moviepy.editor import AudioFileClip
import yaml
from yt_dlp import YoutubeDL


def main():
    snippets_dir = Path("snippets")
    # Create dir, since moviepy doesn't create it.
    snippets_dir.mkdir(parents=True, exist_ok=True)

    config = yaml.safe_load(Path("snippets.yaml").read_text())
    for hash_, snippets in config.items():
        current_video_path = Path(".cache") / f"{hash_}.mp3"

        for snippet in snippets:
            current_snippet_path = snippets_dir / f"{snippet['name']}.mp3"

            # Only process snippet if not already done.
            if current_snippet_path.is_file() and not snippet.get("force", False):
                continue

            # Only download if not already done.
            if not current_video_path.is_file():
                options = {
                    "format": "bestaudio/best",
                    "outtmpl": str(current_video_path),
                    "quiet": True,
                    "no_warnings": True,
                }
                with YoutubeDL(options) as ydl:
                    ydl.download([f"https://www.youtube.com/watch?v={hash_}"])

            # Finally cut the snippet from the full video.
            with AudioFileClip(str(current_video_path)) as audioclip:
                audioclip.subclip(snippet["start"], snippet["end"]).write_audiofile(
                    current_snippet_path, logger=None
                )


if __name__ == "__main__":
    main()
